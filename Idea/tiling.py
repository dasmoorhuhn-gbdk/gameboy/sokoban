w=160
h=144
s=8

x_start = 8
y_start = 16

result = []

sprites_per_x = w / s
sprites_per_y = h / s

print(f"{w}x{h}")
print(f"X{sprites_per_x} Y{sprites_per_y}")

y_last = 0

for y in range(int(sprites_per_y)):
    y += 1
    y_calc = y_start if y == 1 else y_last + x_start
    y_last = y_calc
    row = []
    for x in range(int(sprites_per_x)):
        x += 1
        x_calc = x * x_start
        row.append(f"{x_calc}x{y_calc}")
    result.append(row)
    y_calc = y * y_start

print(len(result))
with open(file="result.csv", mode="w") as file:
    file.write("1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20\n")
    for row in result:
        print(row)
        y_sum = "".join(f"{y}," for y in row)
        file.write(f"{y_sum}\n")
    file.close()




