MAIN_FILE=$1

mkdir -p build/tmp

$GBDKDIR/bin/lcc \
    -Wa-1 \
    -W1-m \
    -Wf--debug \
    -Wl-y \
    -W1-w \
    -DUSE_SFR_FOR_REG \
    -c -o build/tmp/main.o src/$MAIN_FILE.c

$GBDKDIR/bin/lcc \
    -Wa-1 \
    -W1-m \
    -Wf--debug \
    -Wl-y \
    -W1-w \
    -DUSE_SFR_FOR_REG \
    -o build/main.gb build/tmp/main.o

mv build/*.cdb build/tmp
mv build/*.ihx build/tmp
mv build/*.map build/tmp
