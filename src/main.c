#include <stdio.h>
#include <gb/gb.h>

#include "assets/tiles.c"
#include "assets/maps/map_001/background.c"

#include "assets/maps/map_001/map_001.c"

// W=160
// H=144
// 160x144

int main()
{
    set_bkg_data(0, 6, Tiles);
    set_bkg_tiles(0, 0, map_001_backgroundWidth, map_001_backgroundHeight, map_001_background);
    set_sprite_data(0, 6, Tiles);
    set_sprite_tile(0, 5);
    set_sprite_tile(1, 5);
    set_sprite_tile(2, 5);
    set_sprite_tile(3, 5);
    set_sprite_tile(4, 5);
    set_sprite_tile(5, 5);
    set_sprite_tile(6, 4);


    int joypad_current=0, joypad_previous=0;
    int player_x=map_001_player[0], player_y=map_001_player[1];
    move_sprite(6, player_x, player_y);


    for(int i=0; i<=5; i++)
    {
        move_sprite(i, map_001_goal[i][0], map_001_goal[i][1]);
    }


    SHOW_BKG;
    SHOW_SPRITES;
    DISPLAY_ON;

    while (1)
    {
        switch (joypad())
        {
            case J_LEFT:
                scroll_sprite(6, -8,0);
                delay(200);
                break;
            case J_RIGHT:
                scroll_sprite(6, 8,0);
                delay(200);
                break;
            case J_UP:
                scroll_sprite(6, 0,-8);
                delay(200);
                break;
            case J_DOWN:
                scroll_sprite(6, 0,8);
                delay(200);
                break;
        }
    }
}
