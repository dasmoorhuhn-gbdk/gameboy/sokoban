cmake_minimum_required(VERSION 3.26)
project(sokoban C)

set(CMAKE_C_STANDARD 11)

include_directories(/opt/gbdk/include)
include_directories(src/assets)
include_directories(src/assets/maps/map_001)

add_executable(sokoban
        src/assets/maps/map_001/background.c
        src/assets/maps/map_001/background.h
        src/assets/maps/map_001/map_001.c
        src/assets/tiles.c
        src/assets/tiles.h
        src/main.c)
